package com.midhilaj.myapplication;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import net.idik.lib.slimadapter.SlimAdapter;
import net.idik.lib.slimadapter.SlimInjector;
import net.idik.lib.slimadapter.viewinjector.IViewInjector;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    List<Object> data = new ArrayList<>();
    RecyclerView mRecyclerView;
    SlimAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRecyclerView=(RecyclerView)findViewById(R.id.homepagerv);
        LinearLayoutManager  l=new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(l);
        mAdapter= SlimAdapter.create()
                .register(R.layout.layoutimage1, new SlimInjector<MultiImages1>() {
                    @Override
                    public void onInject(MultiImages1 data, IViewInjector injector) {

                    }
                })
                .register(R.layout.layoutimage3, new SlimInjector<MultiImages3>() {
                    @Override
                    public void onInject(MultiImages3 data, IViewInjector injector) {

                    }
                })
                .register(R.layout.layoutimage5, new SlimInjector<String>() {
                    @Override
                    public void onInject(String data, IViewInjector injector) {

                    }
                })
                .registerDefault(R.layout.layoutimage6, new SlimInjector() {
                    @Override
                    public void onInject(Object data, IViewInjector injector) {

                    }
                })
                .attachTo(mRecyclerView);

        for(int i=0;i<100;i++){
            if(i%6==0){
                data.add(new MultiImages6());
            }else if(i%5==0){
                data.add(new MultiImages5());
            }else if(i%3==0){
                data.add(new MultiImages3());
            }else if(i%2==0){
                data.add(new MultiImages1());
            }else {
                data.add(new VideoOnly());
            }
        }
        mAdapter.updateData(data);
    FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

class MultiImages3{
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}
class MultiImages5{
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}
class MultiImages6{
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}
class VideoOnly{
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }
}
class MultiImages1 {
    MainDetails mainDetails;

    public void setMainDetails(MainDetails mainDetails) {
        this.mainDetails = mainDetails;
    }

    public MainDetails getMainDetails() {
        return mainDetails;
    }

}
